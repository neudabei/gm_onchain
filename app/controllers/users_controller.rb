class UsersController < ApplicationController
  def create
    binding.pry
    @user = User.new(user_params)

    if @user.save
      redirect_to root_path
    else
      render :new
    end
  end

  private

  def user_params
    params.require(:user).permit(:public_address, :signature, :twitter_username)
  end
end
