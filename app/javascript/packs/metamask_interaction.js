import { ethers } from "ethers";
import MetaMaskOnboarding from "@metamask/onboarding";
import Rails from "@rails/ujs"

Rails.ajax({
  type: "POST",
  url: "/users",
  data: { user: { public_address: '123' } }
})

window.addEventListener("DOMContentLoaded", () => {
  const onboarding = new MetaMaskOnboarding();
  const onboardButton = document.getElementById("connectButton");
  let accounts;

  const updateButton = () => {
    if (!MetaMaskOnboarding.isMetaMaskInstalled()) {
      onboardButton.innerText = "Click here to install MetaMask!";
      onboardButton.onclick = () => {
        onboardButton.innerText = "Onboarding in progress";
        onboardButton.disabled = true;
        onboarding.startOnboarding();
      };
    } else if (accounts && accounts.length > 0) {
      onboardButton.innerText = "Connected to MetaMask";
      onboardButton.disabled = true;
      onboarding.stopOnboarding();
    } else {
      onboardButton.innerText = "Connect Wallet";
      onboardButton.onclick = async () => {
        await window.ethereum.request({
          method: "eth_requestAccounts"
        });
      };
    }
  };

  updateButton();

  if (MetaMaskOnboarding.isMetaMaskInstalled()) {
    window.ethereum.on("accountsChanged", newAccounts => {
      accounts = newAccounts;
      updateButton();
    });
  }

  // Get and show account address
  const showAccount = document.querySelector('.showAccount');
  getAccount();

  async function getAccount() {
    const accounts = await ethereum.request({ method: 'eth_requestAccounts' });
    const account = accounts[0];
    showAccount.innerHTML = account;
  }

const signTypedDataV3 = document.getElementById('signTypedDataV3');
const signTypedDataV3Result = document.getElementById('signTypedDataV3Result');
const signTypedDataV3Verify = document.getElementById('signTypedDataV3Verify');
const signTypedDataV3VerifyResult = document.getElementById(
  'signTypedDataV3VerifyResult',
);
  const ethSign = document.getElementById('ethSign');
  const ethSignResult = document.getElementById('ethSignResult');
  const personalSign = document.getElementById('personalSign');
  const personalSignResult = document.getElementById('personalSignResult');

});

// const connectWallet = () => {

//   if (typeof window.ethereum !== 'undefined') {
//     const provider = new ethers.providers.Web3Provider(window.ethereum)
//     provider.send("eth_requestAccounts", []); // connect to MetaMask
//   }
//   else {
//     connectButton.innerText = "Please install MetaMask to use this dApp"
//   }

// };

// window.addEventListener('DOMContentLoaded', (event) => {
//   const connectButton = document.querySelector('#connectButton');
//   connectButton.addEventListener("click", connectWallet);
// });
