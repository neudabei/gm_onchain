class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :public_address, unique: true
      t.string :nonce
      t.string :signature
      t.string :twitter_username
      t.timestamps
    end
  end
end
